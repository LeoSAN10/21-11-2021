using System;
using System.ComponentModel.DataAnnotations;

namespace _21_11_2021.Models
{
    public class Training
    {
        [Required(ErrorMessage = "Training name")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Start date")]
        public DateTime Date { get; set; }


        public enum Status
        {
            Completed,
            Planned,
            Recruting,
            InProgress
        }
    }
}
