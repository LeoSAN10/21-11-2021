﻿using System.Collections.Generic;
  
namespace _21_11_2021.Models
{
    public static class Repository 
    {
        public static List<Member> members{ get; set; } = new List<Member>();
        public static IEnumerable<Member> Members => members;

        public static List<Training> trainings { get; set; } = new List<Training>();
        public static IEnumerable<Training> Trainings => trainings;

        public static void AddResponse(Member member) 
        {
            members.Add(member);
        }
    }
}